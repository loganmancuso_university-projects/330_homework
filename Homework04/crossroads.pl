% solution(...) holds for a solution to CROSS + ROADS = DANGER.
solution(C,N,O,S,A,D,N,G,E,R) :- 
	 dig(S),
   R is (S+S) mod 10, C1 is (S+S) // 10, 
   dig(S), dig(D),   
   E is (S+D+C1) mod 10, C10 is (S+D+C1) // 10,
   dig(O), dig(A),
   G is (O+A+C10) mod 10, C100 is (O+A+C10) // 10,
   dig(R), dig(O),
   N is (R+O+C100) mod 10, C1000 is (R+O+C100) // 10,
   dig(C), dig(R), C > 0, R > 0,
   A is (C+R+C1000) mod 10, D is (C+R+C1000) // 10,
   uniq_digits(C,N,O,S,A,D,N,G,E,R).

% uniq(...) holds if the arguments are all distinct digits.
uniq_digits(C,N,O,S,A,D,N,G) :-
   dig(C), dig(N), dig(O), dig(S), dig(A), dig(D), dig(N),	dig(G),	dig(E), dig(R),
   \+ C=N, \+ C=O, \+ C=S, \+ C=A, \+ C=D, \+ C=N, \+ C=G, \+ C=E,	\+ C=R,
           \+ N=O, \+ N=S, \+ N=A, \+ N=D, \+ N=N, \+ N=G, \+ N=E,	\+ N=R,
                   \+ O=S, \+ O=A, \+ O=D, \+ O=N, \+ O=G, \+ O=E,  \+ O=R,
                           \+ S=A, \+ S=D, \+ S=N, \+ S=G, \+ S=E,  \+ S=R,
                                   \+ A=D, \+ A=N, \+ A=G, \+ A=E,  \+ A=R,
                                           \+ D=N, \+ D=G, \+ D=E,  \+ D=R,
                                                   \+ N=G, \+ N=E,  \+ N=R,
                                                   				 \+ G=E,  \+ G=R,
                                                   				 	        \+ E=R.
% The uniq_digits
dig(0). dig(1). dig(2). dig(3). dig(4). 
dig(5). dig(6). dig(7). dig(8). dig(9). 
