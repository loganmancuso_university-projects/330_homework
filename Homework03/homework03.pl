%/****************************************************************
 %* 'homework03.pl'
 %*
 %* author/copyright: mancuso, logan
 %* last edit date: 09-20-2017--15:21:29
 %*
%**/

%/****************************************************************
% * atomic sentences
%**/

%/****************************************************************
% * male/female
%**/

%males (name)
male(a).
male(b).
male(c).
male(d).
male(e).
male(f).
male(g).
male(h).
male(i).
male(m).
male(q).
male(x).
male(y).
male(aa).
male(ac).
male(ae).
male(ad).
male(aj).
male(al).
male(am).

%females (name)
female(j).
female(k).
female(l).
female(n).
female(o). 
female(p).
female(r).
female(s).
female(t).
female(u).
female(v).
female(w).
female(z).
female(ab).
female(ah).
female(ag).
female(ak).
female(an).

%/****************************************************************
% * Children (child,parent)
%**/

%a and j married and have child o and b
child(b,a).
child(o,a).
child(b,j).   
child(o,j).

%k's parents are unknown but married b and has child c
child(c,b).
child(c,k).

%d and n married and had h    
child(h,d).
child(h,n).

%p and e married and have g, r, and u
child(g,p).
child(r,p).
child(u,p).
child(g,e).
child(r,e).
child(u,e).

%o married g and have kids t and i
child(t,o).
child(i,o).
child(t,g).
child(i,g).

%t and h married and have w, l, and v
child(w,t).
child(l,t).
child(v,t).
child(w,h).
child(l,h).
child(v,h).

%c and v married to have q
child(q,c).
child(q,v).

%q and s married to have ac
child(ac,q).
child(ac,s).

%w and aa married to have ab
child(ab,w).
child(ab,qq).

%x and z married to have y and s
child(y,x).
child(y,z).
child(s,x).
child(s,z).

%g and y married to have aa
child(aa,g).
child(aa,y).

%ah and ad married and had aj
child(aj,ah).
child(aj,ad).

%aj and ak married and had al
child(al,aj).
child(al,ak).

%ae and u married and had ag
child(ag,ae).
child(ag,u).

%ad and r married and had af
child(af,ad).
child(af,r).


%k and al married and had am
child(am,k).
child(am,al).

%am and u married and had an
child(an,am).
child(an,u).


%/****************************************************************
% * conditional statements
%**/

opp_sex(X,Y) :- male(X), female(Y). 
opp_sex(X,Y) :- male(Y), female(X). 
parent(X,Y) :- child(Y,X).
father(X,Y) :- child(Y,X), male(X).
mother(X,Y) :- child(Y,X), female(X).

%sister is when a child X, and Y where Y is a female and shares 
% a parent Z with X
sister(X,Y) :- parent(Z,Y), parent(Z,X),
								female(Y), \+X=Y.
%sister is when a child X, and Y where Y is a male and shares 
% a parent Z with X
brother(X,Y) :- child(Y,Z), child(X,Z),
									male(Y), \+X=Y.
%child X and Y share both parents W and Z
full_sibling(X,Y) :- parent(W,X), parent(Z,X), 
											parent(W,Y), parent(Z,Y),
											\+Z=W, \+X=Y.
%child X and Y share at most one parent Z and 
%does not share a parent W, H
half_sibling(X,Y) :- parent(Z,X), parent(Z,Y),
											parent(W,X), parent(H,Y),
											\+W=H, \+Z=H, \+Z=W, \+X=Y.
%two people X,Y are cousins if X has a parent Z with 
%a sibling W and has a child Y
cousin(X,Y) :- parent(Z,X), half_sibling(Z,W),
								child(Y,W), \+X=Y, \+Z=W.
%two people X,Y are first_cousins if X has a parent Z with 
%a full_sibling W and has a child Y
first_cousin(X,Y) :- parent(Z,X), full_sibling(Z,W),
											child(Y,W), \+X=Y, \+Z=W.
%two people X,Y are second_cousins if X has a parent Z with 
%a parent W and has a child H, and has a child Y
second_cousin(X,Y) :- parent(Z,X), parent(W,Z), 
												child(H,W), child(Y,H),
												\+Z=H, \+X=Y, \+Z=W.
%two people X,Y are half_cousins if X has a parent Z and Y 
%has a parent W and the parent Z and W share a parent H 
%but not the other parent K, so at most one parent 
half_cousin(X,Y) :- parent(Z,X), parent(W,Y),
											parent(H,Z), parent(H,W),
											parent(K,Z), parent(J,W),
											\+J=K, \+K=H, \+H=J, \+X=Y.
%double_first_cousin(X,Y) :- .
%X has a parent Z, who has a parent W, who has a parent H,
%who has a sibling K, who has a child Y
first_cousin_twice_removed(X,Y) :- parent(Z,X), parent(W,Z),
																		parent(H,W), full_sibling(H,K),
																		child(Y,K), \+X=Y, 
																		\+K=W, \+H=K, \+Z=H, \+W=Z,
																		\+Z=K.
grand_father(X,Y) :- father(X,Z), parent(Z,Y),
											\+X=Y.
great_grand_father(X,Y) :- father(Z,Y), 
														grand_father(X,Z),
														\+X=Y. 
%grand_mother(X,Y) :- .
%great_grand_mother(X,Y) :- .

%ancestor(X,Y) :- .
%descendant(X,Y) :- . 
%closest_common_ancestor(X,Y) :- .
%write_descendant_chain(X,Y) :- .


%/****************************************************************
% * end 'homework03.pl'
%**/
