% lenghts of lists
% x = 3
exactly_3([_,_,_]).
% x >= 3
at_least_3([_,_,_|_]).
% 0 <= x <= 3
at_most_3([]).
at_most_3([_]).
at_most_3([_,_]).
at_most_3([_,_,_]).

%intersect
%intersect([1,2,3,4],[5,4,1,6])
%intersect([1,2,3,4],[5,6])
intersect(X, Y) :- member(Q, X), member(Q, Y).

%all_intersect
%all_intersect([[1,2,3],[4,5,6]],[3,4]).
%all_intersect([],[3,4]).
%all_intersect([[1,2,3],[1,2,5],[5,4,6]],[3,4]).
all_intersect([],_,[]).
all_intersect(X, Y, [X|R]) :- member(Q, X), intersect(Q, Y), all_intersect(Q, Y, R).
all_intersect([_|Q], Y, R) :- all_intersect(Q, Y, R).
%all_intersect([], _, []).
%all_intersect([X|_], Y, [X|Z]) :- member(E, X), intersect(E, Y), all_intersect(E, Y, Z).
%all_intersect([_|E], Y, Z) :- all_intersect(E, Y, Z).

list_length([], 0).
list_length([_|X], L) :- list_length(X, N) , L is N+1.

%check if X is element in a nested list
member(X, [X|_]).                 %X is first element
member(X, [L|_]) :- member(X, L). %X is member of first element
member(X, [_|T]) :- member(X, T). %X is member of tail