%/****************************************************************
 %* 'homework02.pl'
 %*
 %* Author/CopyRight: Mancuso, Logan
 %* Last Edit Date: 09-07-2017--15:21:29
 %*
%**/

%/****************************************************************
% * Atomic Sentences
%**/

% acted_in(person,movie)  % the person acted in the movie
acted_in(Cate_Blanchett, Babel).
acted_in(Brad_Pitt, Babel).
acted_in(Nathalie_Boltt, District_9).
acted_in(Sharlto_Copley, District_9).
acted_in(Jason_Cope, District_9).
acted_in(Adam_Sandler, Click).
acted_in(Kate_Beckinsale, Click).
acted_in(Christopher_Walken, Click).
acted_in(David_Hasslhoff, Click).
acted_in(Henry_Winkler, Click).
acted_in(Leonardo_DiCaprio, The_Aviator).
acted_in(Cate_Blanchett, The_Aviator).
acted_in(Kate_Beckinsale, The_Aviator).
acted_in(Jennifer_Aniston, Were_The_Millers).
acted_in(Jason_Sudeikis, Were_The_Millers).
acted_in(Emma_Roberts, Were_The_Millers).
acted_in(Jennifer_Aniston, Horrible_Bosses).
acted_in(Jason_Bateman, Horrible_Bosses).
acted_in(Kevin_Spacey, Horrible_Bosses).
acted_in(Jennifer_Aniston, The_Switch).
acted_in(Jason_Bateman, The_Switch).
acted_in(Jeff_Goldblum, The_Switch).
acted_in(Patton_Oswalt, Ratatouille).
acted_in(Ian_Holm, Ratatouille).
acted_in(Lou_Romano, Ratatouille).
acted_in(Samuel_Jackson, The_Incredibles).
acted_in(Holly_Hunter, The_Incredibles).
acted_in(Keanu_Reeves, The_Matrix).
acted_in(Laurence_Fishburne, The_Matrix).
acted_in(Hugo_Weaving, The_Matrix).
acted_in(Leonardo_DiCaprio, Inception).
acted_in(Ellen_Page, Inception).
acted_in(Joseph_Gordon_Levitt, Inception).
acted_in(Tom_Hardy, Inception).
acted_in(Leonardo_DiCaprio, Shutter_Island).
acted_in(Mark_Ruffalo, Shutter_Island).
acted_in(Ben_Kingsley, Shutter_Island).

% directed(person,movie)  % the person directed the movie
directed(Alejandro, Babel).
directed(Neill_Blomkamp, District_9).
directed(Frank_Coraci, Click).
directed(Martin_Scorsese, The_Aviator).
directed(Rawson_Marshall_Thurber, Were_The_Millers).
directed(Seth_Gordon, Horrible_Bosses).
directed(Josh_Gordon, The_Switch).
directed(Will_Speck, The_Switch).
directed(Brad_Bird, Ratatouille).
directed(Jan_Pinkava, Ratatouille).
directed(Brad_Bird, The_Incredibles).
directed(Lana_Wachowski, The_Matrix).
directed(Lilly_Wachowski, The_Matrix).
directed(Christopher_Nolan, Inception).
directed(Martin_Scorsese, Shutter_Island).

% released(movie,year)  % the movie came out that year
released(Babel, 2006).
released(District_9, 2009).
released(Click, 2006).
released(The_Aviator, 2004).
released(Were_The_Millers, 2013).
released(Horrible_Bosses, 2011).
released(The_Switch, 2010).
released(Ratatouille, 2007).
released(The_Incredibles, 2004).
released(The_Matrix, 1999).
released(Inception, 2010).
released(Shutter_Island, 2010).

%/****************************************************************
% * Conditional Statements
%**/

% actor in both X and Y
in_both(X, Y) :- acted_in(Z, X), acted_in(Z, Y).
% find movie released in some date X
released_in(X) :- released(Y, X).
% same director different movie
more_than_one_movie :- directed(X, Y), directed(X, Y).
% different directors same movie
more_than_one_director :- directed(X, Y), directed(Z, Y).
% .
more_than_one_movie_year :- in_both(X, Y), released_in(X, W), released_in(Y, W).
% .
same_director_different_years :- acted_in(X, Y), directed(Z, Y), directed(Z, C), released(Y, W), released(C, Q).

%/****************************************************************
% * End 'homework02.pl'
%**/
